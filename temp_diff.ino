#include <OneWire.h>


// This is an temperature diffent detection.
// in this speical case it's for a "heating", to automatic switch off.
//
// This will be done, if:
//  1) the different between the both Sensors (e.g. DS18S20) are
//  2) for a given time of seconds/minutes high enough
// Then the shutdown-Timer starts (for seconds/minutes).
// The shutdown timer will send the Relais (=the Heating) will switched off.

// Security:
// Be aware on working with high voltage. The problem is to witch the hight voltage. There a few alternative idea's for a more secure way:
//  a) use a Power Outlet Strip, which can be switched on via external USB-Power.
//  b) use a Wireless-Power-Plug. On Wireless-Power-Plug you could work and solder on the remote control (only battery low voltage).
//     Active this Mode in the config.  The you could use this impuls to "press" the "off" button on a wireless-remote control for wireless Power-Plug.
//  c) Take special industry product for external Relais remote (more expensiv). For external Relais input (there are rare product, as i know).


// Status on the build-In-LED (on board):
//  1) off = default/normal/startUp
//  2) On = if Heating is really running (=means temperture Different is detected, big enough)
//  2) blinking (with a few breaks) = while shutDown-Timer is running

// SourceCode:
// Temperature dection is based on the source from a Ardunio-Examples.
//                 URL: https://github.com/soerenj/arduino-temp-diff-sensor-switch/
String sourceCodeUrl = "https://github.com/soerenj/arduino-temp-diff-sensor-switch/";
// URL (based on this example): https://github.com/wemos/D1_mini_Examples/blob/master/examples/03.Sensors/DS18x20_Temperature/DS18x20_Temperature.ino
// Licence (only my parts): APGL; MIT, GPL2/3, CC-0, public domain, or what ever you want (i prevere GPL)

// Hardware:
// Ardunino (or simular/compatible, like wemos, nodemcu,...)
//  - Digital Pin2 push-Button (must be accecpt interrups; normaly not any Pin) (this Pin is not needed in Wireless-Remote-Control--Power-Plug Mode)
//  - Digital Pin3 2x Temperature Sensors like DS18B20 (with 4.7K resistor)
//  - Digital Pin4 Relais-Module.
//                  For the Relais are 2Modes inside the Programm:
//                  a) Relais direct connected to heating/device
//                  b) "wirelessRemoteControlImplus": Wireless-Remote-Control Mode: The "off" button is on a remote-control to plug of the Wireless Power Plug.


// FAQ: Temperature-different is not detected:
//    Try the other way around of the Temperature Sensors. You must try which sensor is for the hot water, with fore the colder water.


// -- config start -- //

const int sleepTimeInSec = 30; //seconds to wait between each measure ; 3600=every 60 Minutes; 300=5Min 
int thresholdTempDiff    = 3; //Temperature Differents: detect as running, if temperature different are at this minimal-size
int durationMinTimeTempDiff = 30; //seconds; detect a running. The temperture differnet musst hold for at least this time. (possible unneded function, but possible preveent measure errors)
int shutdownTime = 900;  //seconds; Wait-Time (duration) before switch of heating

byte buttonStateDefault = LOW; //Should the Heating be on, after blackout? default state at power on of the board (=also default after power blackout); (values: LOW|HIGH)
int wirelessRemoteControlImplus = false; //(default:false) only needed for Mode with wireless-Remote-Control: //it will only send SwitchOff impulse, instead SwitchOff Relais (for "press" off on wireless-remote-control for wireless ppower plug)

// -- config end -- //


OneWire  ds(4);  // Temperature-Sensors on pin D4 (a 4.7K resistor is necessary)
float tempDiff_last;
double tempDiffOverThresholdSince;
long wasOnAtTime; //at this Time-point the Heating registered as "realy" running
int debug = 0; //allowed value 2 (2=hardware-adress of TempSensors)
volatile unsigned long shutdownTimerStarted;


//DIFFFERENECE TO SOURCEOCDE ONLINE:  BUTTON GELÖSCHT / BUTTON DELETED




void setup() {
  
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(10);

  Serial.println(" SourceCodeUrl:  ");
  Serial.println( sourceCodeUrl );
  
  Serial.println("");
  Serial.println(" Parameter:  ");
  Serial.print("sleepTimeInSec:");
  Serial.println( sleepTimeInSec );
  Serial.print("thresholdTempDiff:");
  Serial.println( thresholdTempDiff );
  Serial.print("durationMinTimeTempDiff:");
  Serial.println( durationMinTimeTempDiff );
  Serial.print("shutdownTime:");
  Serial.println( shutdownTime );
  Serial.print("wirelessRemoteControlImplus:");
  Serial.println( wirelessRemoteControlImplus );

}


void loop() {

/* old:
  if(!wirelessRemoteControlImplus && buttonState==false){
    shutdownTimerStarted=0; //stop shutdownTimer
    tempDiffOverThresholdSince = 0;
    wasOnAtTime = 0;
    return; //loop only full running, if switched on via button
  }*/
  delay(10);

  float celsius, celsius2, tempDiff;
  byte addr[8];
  int i;
  String t_id;
  t_id = "";
  
  if(debug==2)Serial.print("ROM =");
  for( i = 0; i < 8; i++) {
    if(debug==2)Serial.write(' ');
    if(debug==2)Serial.print(addr[i], HEX);
    t_id += String(addr[i], HEX);
  }

  if ( !ds.search(addr)) Serial.println("Failed: could not read any TempSensor.");
  else celsius = readTemp(addr);
  if ( !ds.search(addr)) Serial.println("Failed: could read only one TempSensor.");
  else celsius2 = readTemp(addr);
  ds.reset_search();
  
  tempDiff = celsius2-celsius;
  
  Serial.print("TempDiff:  ");
  Serial.print(tempDiff);
  Serial.print("   ( last: ");
  Serial.print(tempDiff_last);
  Serial.println(" )");


  int durationOverTreshold = (millis()-tempDiffOverThresholdSince)/1000;
  
  if(tempDiff>thresholdTempDiff){ //hot
    if(tempDiffOverThresholdSince==0)tempDiffOverThresholdSince=millis();
    
    if(durationOverTreshold>durationMinTimeTempDiff){
          //Serial.print("LED_BUILTIN AN");
          //Serial.print(" seit ");
          //Serial.print(durationOverTreshold);
          //Serial.println(" sekunden");
          digitalWrite(LED_BUILTIN, HIGH);
          wasOnAtTime = millis();
          Serial.print("Heating is accepted as running.");
          //Serial.print("At least since");
          //Serial.print(durationMinTimeTempDiff);
          //Serial.println(" second; LED on");
      }else{
        Serial.print("Heating seems to be running, please wait at least ");
        Serial.print(durationMinTimeTempDiff);
        Serial.println(" seconds");
        }
  }else{ //cold
    if( shutdownTimerStarted==0 && tempDiff_last>thresholdTempDiff){
      Serial.println("shutdown timer started.");
      shutdownTimerStarted=millis();;
    }
    long shutdownTimerEnds = shutdownTimerStarted+(shutdownTime*1000);
    if( shutdownTimerStarted>0 && millis()>shutdownTimerEnds){
      shutdownTimerStarted = 0;
      Serial.println("shutdown timer finshed.");
      if(!wirelessRemoteControlImplus){
        Serial.println("switch Relais to off");
        digitalWrite(3, LOW);
        Serial.println("switch Relais send impuls");  
        digitalWrite(3, HIGH);
        delay(100);
        digitalWrite(3, LOW);
      }
    }
    tempDiffOverThresholdSince = 0;
    digitalWrite(LED_BUILTIN, LOW);
    if(shutdownTimerStarted>0){
      Serial.print("shutdown timer running... ");
      Serial.print( (millis()-shutdownTimerStarted) / 1000 );
      Serial.println( " seconds" );
    }
  }
  
  if( shutdownTimerStarted>0  ){
      int i;
      //LED blinking while shutdown timer
      for(i=0;i<sleepTimeInSec;i++){
        digitalWrite(LED_BUILTIN, HIGH);
        delay(250);
        digitalWrite(LED_BUILTIN, LOW);
        delay(250);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(250);
        digitalWrite(LED_BUILTIN, LOW);
        delay(250);
        digitalWrite(LED_BUILTIN, HIGH);
      }
      digitalWrite(LED_BUILTIN, LOW);
    }else delay(sleepTimeInSec*1000);
    tempDiff_last = tempDiff;

}

float readTemp(byte addr[8]){
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  
  float celsius, fahrenheit;



  if (OneWire::crc8(addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return;
  }
  Serial.println();
 
  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      if(debug==2)Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      if(debug==2)Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      if(debug==2)Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      Serial.println("Device is not a DS18x20 family device.");
      return;
  } 

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  if(debug==2)Serial.print("  Data = ");
  if(debug==2)Serial.print(present, HEX);
  if(debug==2)Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    if(debug==2)Serial.print(data[i], HEX);
    if(debug==2)Serial.print(" ");
  }
  if(debug==2)Serial.print(" CRC=");
  if(debug==2)Serial.print(OneWire::crc8(data, 8), HEX);
  if(debug==2)Serial.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  if(debug==2)Serial.print("  Temperature = ");
  if(debug==2)Serial.print(celsius);

  if(debug==2)Serial.print(" Celsius, ");
  //if(debug==2)Serial.print(fahrenheit);
  //if(debug==2)Serial.println(" Fahrenheit");
  return celsius;
}
