# arduino-temp-diff-sensor-switch

Switch Relais off after ending of Temperature-Different. Just a small personal arduino-Project.

This Version is **not tested**. Last test done with [other Version](https://gitlab.com/soerenj/arduino-temp-diff-sensor-switch/tree/with-switch).

General not finally tested in real world..


Function
* it measure Temperature different _(repetetive; between two sensors)_.
* first Action : Temperature is growing. OK; _Waiting for next:_
* second Action : Temperature falling or negativ : _Start Coutdown:_
* countdown finished: switch off relais ( _target_ device )
* [Only in other Version](https://gitlab.com/soerenj/arduino-temp-diff-sensor-switch/tree/with-switch): press On-Button. This will switch on the Power for the relais ( _target_ Device e.g. via home heating)



_In my special case: I have a pellet wood heating. After the heating ends, the circulating heatpump wont stop. This board will hard Power off (black out), after a few minutes_

![circuit board layout](board_layout/temp_diff_layout.png)
[pdf](board_layout/temp_diff_layout.pdf)


![circuit board layout alternative](board_layout/temp_diff_layout_alternativ.png)
[pdf](board_layout/temp_diff_layout_alternativ.pdf)

![circuit board layout alternative 2](board_layout/temp_diff_layout_alternativ_z2_ardunio_micro.png)
